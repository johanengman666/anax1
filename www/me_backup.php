<?php 

require __DIR__.'/config_with_app.php'; //test

 

$app->theme->configure(ANAX_APP_PATH . 'config/theme_me.php');

 

$app->router->add('', function() use ($app) {

	$app->theme->setVariable('title', "Hello World Pagecontroller")

			   ->setVariable('main', "

		<h1>Hello World Pagecontroller - Me.php</h1>

		<p>This is a sample pagecontroller that shows how to use Anax.</p>

	");

});

 

$app->router->add('redovisning', function() use ($app) {

$app->theme->setTitle("Redovisning");

    $app->views->add('me/redovisning');

/*

	$app->theme->setVariable('title', "Hello World Pagecontroller")

			   ->setVariable('main', "

		<h1>Hello World Pagecontroller - Me.php/redovisning</h1>

		<p>This is a sample pagecontroller that shows how to use Anax.</p>

	");

	*/

});

 

$app->router->add('source', function() use ($app) {

	$app->theme->setVariable('title', "Hello World Pagecontroller")

			   ->setVariable('main', "

		<h1>Hello World Pagecontroller - Me.php/source</h1>

		<p>This is a sample pagecontroller that shows how to use Anax.</p>

	");

});

 

$app->navbar->configure(ANAX_APP_PATH . 'config/navbar_me.php');

 

$app->router->handle();

$app->theme->render();

/**

 * This is a Anax pagecontroller.

 *

 */

/*

// Get environment & autoloader and the $app-object.

require __DIR__.'/config_with_app.php'; 



echo __DIR__;



// Prepare the page content

$app->theme->setVariable('title', "Hello World Pagecontroller")

           ->setVariable('main', "

    <h1>Hello World Pagecontroller - Me.php</h1>

    <p>This is a sample pagecontroller that shows how to use Anax.</p>

");







// Render the response using theme engine.

$app->theme->render();

*/

