<?php 
/**
 * This is a Anax pagecontroller.
 *
 */

// Get environment & autoloader and the $app-object.
require __DIR__.'/config_with_app.php'; 

$app->theme->configure(ANAX_APP_PATH . 'config/theme_me.php');//ändrad ifrån we

$app->theme->setVariable('title', "Hello World Pagecontrollller")
           ->setVariable('main', "
    <h1>Hello World Pagecontroller</h1>
    <p>This is a sample pagecontroller that shows how to use Anax.</p>
");
//$content = $app->fileContent->get('we.md');

//echo __DIR__;
 
$app->router->add('', function() use ($app) {
 //******************
     $app->theme->setTitle("Titel?");
 
    $content = $app->fileContent->get('me.md');//hämtar .md-filen//ändrad ifrån we
    $content = $app->textFilter->doFilter($content, 'shortcode, markdown');
 
    $byline = $app->fileContent->get('byline.md');//vad sjutton gör denna???
    $byline = $app->textFilter->doFilter($byline, 'shortcode, markdown');
 
    $app->views->add('me/page', [//ändrad ifrån we
        'content' => $content,//lägger till .md-filen
        'byline' => $byline,
    ]);
 //******************
});
 
//**************************************
$app->router->add('redovisning', function() use ($app) {//sätter "länken", ex we.php/test
 
    $app->theme->setTitle("Redovisning");//sätter titeln
 
    $content = $app->fileContent->get('redovisning.md');//hämtar .md-filen
    $content = $app->textFilter->doFilter($content, 'shortcode, markdown');
 
    $byline = $app->fileContent->get('byline.md');//vad sjutton gör denna???
    $byline = $app->textFilter->doFilter($byline, 'shortcode, markdown');
 
    $app->views->add('me/page', [//ändrad ifrån we
        'content' => $content,//lägger till .md-filen
        'byline' => $byline,
    ]);
 
});
//**************************************

//**************************************
$app->router->add('about', function() use ($app) {//sätter "länken", ex we.php/test
 
    $app->theme->setTitle("About");//sätter titeln
 
    $content = $app->fileContent->get('about.md');//hämtar .md-filen
    $content = $app->textFilter->doFilter($content, 'shortcode, markdown');
 
    $byline = $app->fileContent->get('byline.md');//vad sjutton gör denna???
    $byline = $app->textFilter->doFilter($byline, 'shortcode, markdown');
 
    $app->views->add('me/page', [//ändrad ifrån we
        'content' => $content,//lägger till .md-filen
        'byline' => $byline,
    ]);
 
});
//**************************************
 
$app->router->add('source', function() use ($app) {
 
    $app->theme->addStylesheet('css/source.css');
    $app->theme->setTitle("Källkod");
 
    $source = new \Mos\Source\CSource([
        'secure_dir' => '.', 
        'base_dir' => '.', 
        'add_ignore' => ['.htaccess'],
    ]);
 
    $app->views->add('me/page', [//ändrad ifrån we
        'content' => $source->View(),
    ]);
 
});

$app->navbar->configure(ANAX_APP_PATH . 'config/navbar_me.php');//ändrad ifrån we
 
$app->router->handle();
$app->theme->render();
